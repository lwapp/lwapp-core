package org.lwapp.core.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Interceptor
public class AuditLogInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(AuditLogInterceptor.class);

    @AroundInvoke
    public Object aroundInvoke(final InvocationContext context) throws Exception {
        final long startTime = System.currentTimeMillis();
        try {
            return context.proceed();
        } finally {
            LOG.info("{}.{} took {} ms", context.getMethod().getDeclaringClass(), context.getMethod().getName(), System.currentTimeMillis() - startTime);
        }
    }
}
