/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.lwapp.core.rest.ws.rest;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.Marshaller;

import org.apache.cocoon.pipeline.CachingPipeline;
import org.apache.cocoon.pipeline.Pipeline;
import org.apache.cocoon.sax.SAXPipelineComponent;
import org.apache.cocoon.sax.component.XIncludeTransformer;
import org.apache.cocoon.sax.component.XMLGenerator;
import org.apache.cocoon.sax.component.XMLSerializer;
import org.apache.cocoon.sax.component.XSLTTransformer;
import org.glassfish.jersey.server.wadl.WadlApplicationContext;

import com.sun.research.ws.wadl.Application;

@Produces({ MediaType.TEXT_HTML, MediaType.APPLICATION_XML })
@Path("api")
public class Wadl2HtmlResource {

	@Context
	private WadlApplicationContext wadlContext;
	@Context
	private UriInfo uriInfo;
	@Context
	private ServletContext servletContext;

	public byte[] applicationWADL() throws Exception {
		final ByteArrayOutputStream baos;
		final Pipeline<SAXPipelineComponent> pipeline = new CachingPipeline<SAXPipelineComponent>();

		final Application application = wadlContext.getApplication(uriInfo, false).getApplication();
		/* 
		 * Get the current XML WADL into a byte array. 
		 */
		final Marshaller marshaller = wadlContext.getJAXBContext().createMarshaller();
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		marshaller.marshal(application, os);
		final byte[] wadlXmlRepresentation = os.toByteArray();

		final XMLGenerator pipelineComponent = new XMLGenerator(wadlXmlRepresentation);
		pipeline.addComponent(pipelineComponent);
		pipeline.addComponent(new XSLTTransformer(getClass().getResource("/prepare-includenew.xsl")));
		pipeline.addComponent(new XIncludeTransformer(getClass().getResource("/")));
		pipeline.addComponent(XMLSerializer.createXMLSerializer());

		baos = new ByteArrayOutputStream();
		pipeline.setup(baos);

		pipeline.execute();

		return baos.toByteArray();
	}

	@GET
	public Response doGet(@Context final HttpServletRequest request, @Context final HttpServletResponse response) throws Exception {

		final Pipeline<SAXPipelineComponent> pipeline = new CachingPipeline<SAXPipelineComponent>();
		pipeline.addComponent(new XMLGenerator(applicationWADL()));

		final XSLTTransformer xslt = new XSLTTransformer(getClass().getResource("/indexnew.xsl"));

		final Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("contextPath", request.getContextPath());
		parameters.put("application", "application");
		xslt.setParameters(parameters);

		pipeline.addComponent(xslt);

		pipeline.addComponent(XMLSerializer.createHTML4Serializer());
		pipeline.setup(response.getOutputStream());
		pipeline.execute();

		return Response.ok().build();
	}

	@GET
	@Path("schema")
	public Response schema(@Context final HttpServletRequest request, @Context final HttpServletResponse response) throws Exception {

		final Pipeline<SAXPipelineComponent> pipeline = new CachingPipeline<SAXPipelineComponent>();

		pipeline.addComponent(new XMLGenerator(applicationWADL()));

		final XSLTTransformer schemaXSLT = new XSLTTransformer(getClass().getResource("/schemanew.xsl"));

		final Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("contextPath", request.getContextPath());
		parameters.put("schema-position", 1);
		parameters.put("schema-prefix", "schema");
		schemaXSLT.setParameters(parameters);

		pipeline.addComponent(schemaXSLT);

		pipeline.addComponent(XMLSerializer.createHTML4Serializer());
		pipeline.setup(response.getOutputStream());
		pipeline.execute();

		return Response.ok().build();
	}

	@GET
	@Path("schema/json")
	public Response schemaJson(@Context final HttpServletRequest request, @Context final HttpServletResponse response) throws Exception {

		final Pipeline<SAXPipelineComponent> pipeline = new CachingPipeline<SAXPipelineComponent>();

		pipeline.addComponent(new XMLGenerator(applicationWADL()));

		final XSLTTransformer schemaXSLT = new XSLTTransformer(getClass().getResource("/jsonxsd.xsl"));

		final Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("contextPath", request.getContextPath());
		parameters.put("schema-position", 1);
		parameters.put("schema-prefix", "schema");
		schemaXSLT.setParameters(parameters);

		pipeline.addComponent(schemaXSLT);

		pipeline.addComponent(XMLSerializer.createHTML4Serializer());
		pipeline.setup(response.getOutputStream());
		pipeline.execute();

		return Response.ok().build();
	}

}
