package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionMapper.class);

	@Override
	public Response toResponse(final Exception ex) {
		LOG.error("Exception occured.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("SYSTEM.ERR")//
				.errorMessage(StringUtils.defaultString(ex.getMessage(), ex.toString()))//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
