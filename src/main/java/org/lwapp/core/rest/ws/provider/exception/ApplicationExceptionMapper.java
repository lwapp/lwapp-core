package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.lwapp.commons.ErrorResponse;
import org.lwapp.commons.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationExceptionMapper.class);

	@Override
	public Response toResponse(final ApplicationException ex) {
		LOG.error("ApplicationException occured.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("APP.ERR")//
				.errorMessage(ex.getMessage())//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
