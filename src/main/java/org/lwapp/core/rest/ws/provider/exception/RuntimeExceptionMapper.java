package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {
	private static final Logger LOG = LoggerFactory.getLogger(RuntimeExceptionMapper.class);

	@Override
	public Response toResponse(final RuntimeException ex) {
		LOG.error("RuntimeException occured.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("RUNTIME.ERR")//
				.errorMessage(StringUtils.defaultString(ex.getMessage(), ex.toString()))//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
