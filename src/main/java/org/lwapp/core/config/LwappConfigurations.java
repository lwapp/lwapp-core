package org.lwapp.core.config;

import org.lwapp.configclient.Configurable;
import org.lwapp.configclient.Configuration;

public enum LwappConfigurations implements Configurable {

    HTTP_PORT("application.invoke.http.port", "4141"), //
    HTTPS_PORT("application.invoke.https.port"), //
    KEY_STORE_PATH("application.keystore.path", "/var/opt/lwapp/ssl/keystore"), //
    KEY_STORE_PASS("application.key.store.password", true), //
    KEY_MANAGED_PASS("application.key.managed.password", true), //
    TRUST_STORE_PASS("application.trust.store.password", true), //
    ADMIN_PORT("application.invoke.admin.port", "11111"), //
    IS_WEB_APP("application.is.web.application", "false"), //
    APP_ENVIRONMENT("application.environment", "DEV", "Identify the system type in which the application is deployed ('DEV','TEST' or 'PROD')"), //
    ;

    private final Configuration config;

    private LwappConfigurations(final String propertyName, final boolean secure) {
        this(propertyName, null, null, secure);
    }

    private LwappConfigurations(final String propertyName, final String defaultValue, final String desc) {
        this(propertyName, defaultValue, desc, false);
    }

    private LwappConfigurations(final String propertyName) {
        this(propertyName, null, null, false);
    }

    LwappConfigurations(final String propertyName, final String defaultValue) {
        this(propertyName, defaultValue, null, false);
    }

    private LwappConfigurations(final String propertyName, final String defaultValue, final String description, final boolean secure) {
        config = new Configuration.Builder().propertyValue(defaultValue).propertyName(propertyName).description(description).secure(secure).build();
    }

    @Override
    public Configuration getConfiguration() {
        return config;
    }

}
