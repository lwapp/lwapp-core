package org.lwapp.core.rest.ws.filter;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

@WebFilter(urlPatterns = "/*", dispatcherTypes = { DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ASYNC, DispatcherType.ERROR, DispatcherType.INCLUDE })
public class LogTraceIdFilter implements Filter {

	private static AtomicLong counter = new AtomicLong();
	private static final Logger LOG = LoggerFactory.getLogger(LogTraceIdFilter.class);

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		final long startTime = System.currentTimeMillis();
		try {
			MDC.put("logTraceId", Long.toString(counter.incrementAndGet() + System.nanoTime()));
			LOG.info("Operation:{}", request);
			chain.doFilter(request, response);
		} finally {
			LOG.info("Total time taken to complete the operation {} ms", System.currentTimeMillis() - startTime);
			MDC.remove("logTraceId");
		}
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}
}