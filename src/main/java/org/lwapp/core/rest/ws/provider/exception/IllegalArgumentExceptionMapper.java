package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {
	private static final Logger LOG = LoggerFactory.getLogger(IllegalArgumentExceptionMapper.class);

	@Override
	public Response toResponse(final IllegalArgumentException ex) {
		LOG.error("IllegalArgumentException occured.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("ILLEGAL.ARGS")//
				.errorMessage(ex.getMessage())//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
