package org.lwapp.core.rest.ws.filter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.validation.ParameterNameProvider;
import javax.validation.Validation;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ContextResolver;

import org.glassfish.jersey.server.validation.ValidationConfig;
import org.glassfish.jersey.server.validation.internal.InjectingConstraintValidatorFactory;

/**
 * Custom configuration of validation. This configuration defines custom:
 * <ul>
 * <li>ConstraintValidationFactory - so that validators are able to inject
 * Jersey providers/resources.</li>
 * <li>ParameterNameProvider - if method input parameters are invalid, this
 * class returns actual parameter names instead of the default ones (
 * {@code arg0, arg1, ..})</li>
 * </ul>
 */
// https://jersey.java.net/documentation/latest/bean-validation.html
public class ValidationConfigurationContextResolver implements ContextResolver<ValidationConfig> {

	@Context
	private ResourceContext resourceContext;

	@Override
	public ValidationConfig getContext(final Class<?> type) {
		return new ValidationConfig().constraintValidatorFactory(resourceContext.getResource(InjectingConstraintValidatorFactory.class))
				.parameterNameProvider(new CustomParameterNameProvider());
	}

	/**
	 * See ContactCardTest#testAddInvalidContact.
	 */
	private class CustomParameterNameProvider implements ParameterNameProvider {

		private final ParameterNameProvider nameProvider;

		public CustomParameterNameProvider() {
			nameProvider = Validation.byDefaultProvider().configure().getDefaultParameterNameProvider();
		}

		@Override
		public List<String> getParameterNames(final Constructor<?> constructor) {
			return nameProvider.getParameterNames(constructor);
		}

		@Override
		public List<String> getParameterNames(final Method method) {
			if (method.getName().startsWith("add")) {
				return Arrays.asList(method.getName().substring(2).toLowerCase());
			}
			return nameProvider.getParameterNames(method);
		}
	}
}