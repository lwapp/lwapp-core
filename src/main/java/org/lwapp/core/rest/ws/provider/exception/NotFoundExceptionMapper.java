package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
	private static final Logger LOG = LoggerFactory.getLogger(NotFoundExceptionMapper.class);

	@Override
	public Response toResponse(final NotFoundException ex) {
		LOG.error("URL not found.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("BAD.URL.ERR")//
				.errorMessage(StringUtils.defaultString(ex.getMessage() + " Please make sure to use correct context. Please check the API document.", ex.toString()))//
				.build();
		return Response.status(Response.Status.NOT_FOUND).entity(errorResponse).build();
	}

}
