package org.lwapp.core.rest.ws.rest;

import java.util.Date;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.lwapp.configclient.ApplicationProperties;
import org.lwapp.core.config.ApplicationServerConfig;
import org.lwapp.core.interceptors.AuditLogInterceptor;

@Path("application")
@Interceptors(AuditLogInterceptor.class)
public class ApplicationResource {

	@Inject
	protected ApplicationServerConfig applicationServerConfig;

	@GET
	public String ping() {
		return "System is ALIVE :" + new Date();
	}

	@GET
	@Path("configurations")
	@Produces({ MediaType.APPLICATION_XML })
	public Response configurations() throws Exception {
		final ApplicationProperties configurations = new ApplicationProperties();
		configurations.getConfigurations().addAll(applicationServerConfig.getAllConfigurations());
		return Response.ok(configurations).build();
	}

}
