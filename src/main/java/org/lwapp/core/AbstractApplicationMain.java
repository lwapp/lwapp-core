
package org.lwapp.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.LowResourceMonitor;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.jboss.weld.exceptions.IllegalArgumentException;
import org.lwapp.commons.cli.AbstractCommand;
import org.lwapp.commons.cli.CliBuilder;
import org.lwapp.commons.cli.CliClientHandler;
import org.lwapp.commons.cli.CliServerFactory;
import org.lwapp.commons.cli.TelnetClientHandler;
import org.lwapp.commons.cli.Terminal;
import org.lwapp.commons.utils.ClassUtils;
import org.lwapp.configclient.AutoStartable;
import org.lwapp.core.config.ApplicationServerConfig;
import org.lwapp.core.config.LwappConfigurations;
import org.lwapp.core.rest.ws.filter.LogTraceIdFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractApplicationMain {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractApplicationMain.class);

    private Server server;
    @Inject
    protected ApplicationServerConfig applicationServerConfig;
    protected static final Terminal terminal = new Terminal();

    public static void main(final String... args) throws Exception {
        try {
            StartMain.main(args);
            LOG.info("If the application does not run, then please make sure you have an empty beans.xml file in your META-INF folder "
                    + "under 'src/main/resources/META-INF' folder. \nThis is because we are using CDI powered by WELD ('http://weld.cdi-spec.org/')");
        } catch (final Exception e) {
            LOG.error("Could not able to start the application.\n" +
                    "Please make sure you have an empty beans.xml file in your META-INF folder under 'src/main/resources/META-INF' folder. "
                    + "\nThis is because we are using CDI powered by WELD ('http://weld.cdi-spec.org/')", e);
            System.exit(-1);
            throw e;
        }
    }

    @Inject
    private void initServices(@Any final Instance<AutoStartable> services) throws Exception {
        for (final AutoStartable service : services) {
            service.init();
        }
    }

    /**
     * @return contextPath The context path
     **/
    protected abstract String getUrlContext();

    /**
     * @return webApp The URL or filename of the webapp directory or war file.
     **/
    protected String getWebApp() {
        return "src/main/webapp/";
    }

    /**
     * Get array of application packages to load.
     * @return ApplicationPackagesToLoad array
     **/
    protected String[] getApplicationPackagesToLoad() {
        return applicationServerConfig.getApplicationPackagesToLoad();
    }

    /**
     * Override the method in case of mapping of new servlets.
     * @return The key value pair for mapped servlet url and servlet class.
     **/
    protected Map<String, Class<? extends HttpServlet>> getServlets() {
        return Collections.emptyMap();
    }

    /**
     * Override the method in case of adding new beans to the server.
     * @return List of Object The list of beans to be added to the server.
     **/
    protected List<Object> getBeans() {
        return Collections.emptyList();
    }

    /**
     * Override this method if you want to perform operation during application
     * startup.
     */
    protected void initApplication() {
    }

    /**
     * Override this method if you want to perform any operation after application server
     * started.
     */
    protected void postApplicationStartup() {
    }

    /**
     * Override this method if you want to perform operation before application server
     * startup.
     * @param server Jetty server instance.
     */
    protected void preApplicationStartup(final Server server) {
    }

    /**
     * Override this method if you want to perform any resource clean up while
     * shutting down
     */
    protected void shutdown() {
    }

    public final void start(@Observes final ContainerInitialized event) throws Exception {
        try {
            startApplication();
        } catch (final Exception e) {
            LOG.error("Could not able to start the application.\n" +
                    "Please make sure you have an empty beans.xml file in your META-INF folder under 'src/main/resources/META-INF' folder. "
                    + "\nThis is because we are using CDI powered by WELD ('http://weld.cdi-spec.org/')", e);
            System.exit(-1);
            throw e;
        }
    }

    private void startApplication() throws Exception {
        initApplication();
        configureJettyServer(terminal);
        preApplicationStartup(server);
        LOG.info("Starting server....");
        server.start();
        startCli();
        postApplicationStartup();
        applicationServerConfig.updateApplicationProperties();
        addShutdownHook(server);
        printSuccessFullStartUp();
    }

    private void printSuccessFullStartUp() {
        final Integer httpsPort = applicationServerConfig.getHttpsPort();
        final String pingUrl;
        if (httpsPort != null) {
            pingUrl = "https://localhost:" + httpsPort + "/" + getUrlContext() + "/application";
        } else {
            pingUrl = "http://localhost:" + applicationServerConfig.getHttpPort() + "/" + getUrlContext() + "/application";
        }
        LOG.info("Server started succussfully.");
        LOG.info("Ping URL: " + pingUrl);

        final String apiDocUrl;
        if (httpsPort != null) {
            apiDocUrl = "https://localhost:" + httpsPort + "/" + getUrlContext() + "/api";
        } else {
            apiDocUrl = "http://localhost:" + applicationServerConfig.getHttpPort() + "/" + getUrlContext() + "/api";
        }
        LOG.info("API Doc: " + apiDocUrl);
    }

    private void startCli() throws Exception {
        final Integer adminPort = applicationServerConfig.getAdminPort();
        if (adminPort == null) {
            LOG.info("No cli is enabled.");
            return;
        }
        final CliBuilder cb = new CliBuilder();
        final Set<Class<? extends AbstractCommand>> cliClasses = ClassUtils.findSubClasses(AbstractCommand.class);
        for (final Class<? extends AbstractCommand> clazz : cliClasses) {
            LOG.info("Starting admin cli: " + clazz.getName());
            cb.addCommands(clazz.newInstance());
        }

        final CliClientHandler cli = new TelnetClientHandler(StringUtils.capitalize(getUrlContext()), cb.createCli());
        LOG.info("Starting admin cli at port: " + adminPort);
        CliServerFactory.startCli(adminPort, cli);
    }

    private void configureJettyServer(final Terminal terminal) throws Exception {
        createServerAndConfigureContext();

        final Integer httpsPort = applicationServerConfig.getHttpsPort();
        final Integer httpPort = applicationServerConfig.getHttpPort();
        if ((httpsPort == null) && (httpPort == null)) {
            throw new IllegalArgumentException("Please provide atleast one of the config properties: " + LwappConfigurations.HTTPS_PORT.getConfiguration().getPropertyName()
                    + "\n OR " + LwappConfigurations.HTTP_PORT.getConfiguration().getPropertyName());
        }
        // HTTPS Configuration
        final HttpConfiguration http_config = new HttpConfiguration();
        http_config.setOutputBufferSize(32768);
        http_config.setRequestHeaderSize(8192);
        http_config.setResponseHeaderSize(8192);
        http_config.setSendServerVersion(true);
        http_config.setSendDateHeader(false);

        // === jetty-http.xml ===

        if (httpPort != null) {
            final ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(http_config));
            http.setPort(httpPort);
            http.setIdleTimeout(30000);
            server.addConnector(http);
        }

        if (httpsPort != null) {
            final ServerConnector https = configureHttpsConnector(terminal, httpsPort, http_config);
            server.addConnector(https);
        }

        // === jetty-lowresources.xml ===
        final LowResourceMonitor lowResourcesMonitor = new LowResourceMonitor(server);
        lowResourcesMonitor.setPeriod(1000);
        lowResourcesMonitor.setLowResourcesIdleTimeout(200);
        lowResourcesMonitor.setMonitorThreads(true);
        lowResourcesMonitor.setMaxConnections(0);
        lowResourcesMonitor.setMaxMemory(0);
        lowResourcesMonitor.setMaxLowResourcesTime(5000);
        server.addBean(lowResourcesMonitor);
    }

    private ServerConnector configureHttpsConnector(final Terminal terminal, final int httpsPort, final HttpConfiguration http_config) {
        http_config.setSecureScheme("https");
        http_config.setSecurePort(httpsPort);
        // SSL Context Factory
        final SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setKeyStorePath(applicationServerConfig.getKeyStorePath());

        String keyStorePassword = applicationServerConfig.getKeyStorePassword();
        if (StringUtils.isBlank(keyStorePassword)) {
            keyStorePassword = terminal.readPassword("Please enter keystore  password:");
        }
        String keyManagedPassword = applicationServerConfig.getKeyManagedPassword();
        if (StringUtils.isBlank(keyManagedPassword)) {
            keyManagedPassword = terminal.readPassword("Please enter key managed password:");
        }

        sslContextFactory.setKeyStorePassword(keyStorePassword);
        sslContextFactory.setKeyManagerPassword(keyManagedPassword);

        sslContextFactory.setTrustStorePath(applicationServerConfig.getKeyStorePath());
        String trustStorePassword = applicationServerConfig.getTrustStorePassword();
        if (StringUtils.isBlank(trustStorePassword)) {
            trustStorePassword = terminal.readPassword("Please enter truststore password:");
        }
        sslContextFactory.setTrustStorePassword(trustStorePassword);

        sslContextFactory.setExcludeCipherSuites("SSL_RSA_WITH_DES_CBC_SHA", //
                "SSL_DHE_RSA_WITH_DES_CBC_SHA", //
                "SSL_DHE_DSS_WITH_DES_CBC_SHA", //
                "SSL_RSA_EXPORT_WITH_RC4_40_MD5", //
                "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", //
                "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", //
                "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA");

        // SSL HTTPS Configuration
        final HttpConfiguration https_config = new HttpConfiguration(http_config);
        final SecureRequestCustomizer src = new SecureRequestCustomizer();
        src.setStsMaxAge(2000);
        src.setStsIncludeSubDomains(true);
        https_config.addCustomizer(src);

        // SSL Connector
        final SslConnectionFactory sslConnectionFactory = new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.asString());
        final HttpConnectionFactory httpConnectionFactory = new HttpConnectionFactory(https_config);
        final ServerConnector https = new ServerConnector(server, sslConnectionFactory, httpConnectionFactory);
        https.setPort(httpsPort);
        https.setIdleTimeout(500000);
        return https;
    }

    private void createServerAndConfigureContext() {
        final QueuedThreadPool threadPool = new QueuedThreadPool();
        server = new Server(threadPool);
        server.setDumpAfterStart(false);
        server.setDumpBeforeStop(false);
        server.setStopAtShutdown(true);

        for (final Object bean : getBeans()) {
            if (bean != null) {
                server.addBean(bean);
            }
        }

        final ServletContextHandler context = createServletContextHandler();
        server.setHandler(context);
    }

    private ServletContextHandler createServletContextHandler() {
        final String urlContext = getUrlContext();
        Validate.notBlank(urlContext, "Please provide urlContext.");

        final ServletContextHandler context;
        if (applicationServerConfig.isWebApp()) {
            LOG.info("Creating WebAppContext");
            Validate.notBlank(getWebApp(),
                    "Please provide 'getWebApp()' (The URL or filename of the webapp directory or war file.) i.e 'src/main/webapp'. Make sure the path exists");
            context = new WebAppContext(getWebApp(), "/" + urlContext);
        } else {
            LOG.info("Creating  ServletContextHandler");
            context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/" + urlContext);
            context.addServlet(configureJerseyResources(), "/*");
        }

        context.getSessionHandler().getSessionManager().setMaxInactiveInterval(60);
        LOG.info("Session Max Interval:{}", context.getSessionHandler().getSessionManager().getMaxInactiveInterval());

        for (final Entry<String, Class<? extends HttpServlet>> entry : getServlets().entrySet()) {
            context.addServlet(entry.getValue(), entry.getKey());
        }

        context.addFilter(LogTraceIdFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
        return context;
    }

    private ServletHolder configureJerseyResources() {
        final ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true)//
                .property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);

        // Register Rest Resources
        final String[] applicationPackagesToLoad = getApplicationPackagesToLoad();
        resourceConfig.packages(applicationPackagesToLoad);
        LOG.info("ApplicationPackagesToLoad:{}", Arrays.asList(applicationPackagesToLoad));

        // Register Other Features
        resourceConfig.register(JacksonFeature.class);

        final ServletContainer servletContainer = new ServletContainer(resourceConfig);
        return new ServletHolder(servletContainer);
    }

    private void addShutdownHook(final Server server) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                LOG.info("Shutting down application..");
                try {
                    if ((server != null) && !server.isStopped()) {
                        server.stop();
                    }
                    shutdown();
                    LOG.info("Application terminated successfully. Bye");
                } catch (final Exception e) {
                    LOG.error("Graceful shutdown went wrong. SIGKILL (kill -9) if you want.", e);
                }
            }
        });
    }

}
