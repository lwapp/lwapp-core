package org.lwapp.core.config;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.lwapp.configclient.Configuration;
import org.lwapp.configclient.client.ConfigurationServiceClient;

@Singleton
public class ApplicationServerConfig {

    @Inject
    private ConfigurationServiceClient configurationServiceClient;

    public Integer getHttpPort() {
        return configurationServiceClient.getInt(LwappConfigurations.HTTP_PORT);
    }

    public Integer getHttpsPort() {
        return configurationServiceClient.getInt(LwappConfigurations.HTTPS_PORT);
    }

    public Integer getAdminPort() {
        return configurationServiceClient.getInt(LwappConfigurations.ADMIN_PORT);
    }

    public String getKeyStorePath() {
        return configurationServiceClient.getString(LwappConfigurations.KEY_STORE_PATH);
    }

    public String[] getApplicationPackagesToLoad() {
        return configurationServiceClient.getApplicationPackagesToLoad();
    }

    public Set<Configuration> getAllConfigurations() {
        return configurationServiceClient.getAllConfigurations();
    }

    public boolean isWebApp() {
        return configurationServiceClient.getBoolean(LwappConfigurations.IS_WEB_APP);
    }

    public String getKeyStorePassword() {
        return configurationServiceClient.getString(LwappConfigurations.KEY_STORE_PASS);
    }

    public String getKeyManagedPassword() {
        return configurationServiceClient.getString(LwappConfigurations.KEY_MANAGED_PASS);
    }

    public String getTrustStorePassword() {
        return configurationServiceClient.getString(LwappConfigurations.TRUST_STORE_PASS);
    }

    public void updateApplicationProperties() throws Exception {
        configurationServiceClient.updateApplicationProperties();
    }

}
