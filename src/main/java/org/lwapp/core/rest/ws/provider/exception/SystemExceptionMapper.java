package org.lwapp.core.rest.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class SystemExceptionMapper implements ExceptionMapper<Throwable> {
	private static final Logger LOG = LoggerFactory.getLogger(SystemExceptionMapper.class);

	@Override
	public Response toResponse(final Throwable ex) {
		LOG.error("Unexpected system occured.", ex);
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("SYSTEM.ERR")//
				.errorMessage("System Error. Contact technical support.")//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
